﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bar : MonoBehaviour
{
    // Start is called before the first frame update
    public int allProgress;
    public int[] done;
    float hasil;
    float theRealHasil;
    public Database db;
    public int day;


    public GameObject[] bar;
    public GameObject[] bar2;

    public Text[] dayAndProgress;

    void Start()
    {
        //day = findobjectoftype.Database



        db = FindObjectOfType<Database>();

    }

    // Update is called once per frame
    void Update()
    {
            allProgress = db.mirrorSprints[db.currentSprint];
            day = db.dayCounter;
            done[day-1] = db.sprints[db.currentSprint];
        
       if(db.dayCounter < 11)

        {   bar2[db.dayCounter-1].SetActive(true);
            for(int i=0; i < db.dayCounter; i++)
            {
                hasil = done[i];
                theRealHasil = hasil / allProgress;
                

                bar[i].transform.localScale = new Vector3(1, theRealHasil, 1);

                dayAndProgress[i].text = "" + (i) + "\n" + (theRealHasil * 100).ToString("F0") + "%";


            }
        }

       if(db.dayCounter >= 11)
        {
            int counterBar = 0;
            for(int i=db.dayCounter-10; i<=db.dayCounter; i++)
            {

                hasil = done[i];
                theRealHasil = hasil / allProgress;

                bar[counterBar].transform.localScale = new Vector3(1, theRealHasil, 1);

                dayAndProgress[counterBar].text = "" + (i) + "\n" + (theRealHasil * 100).ToString("F0") + "%";
                counterBar++;

            }
            counterBar = 0;
        }
      
    }

    void hitung(int i)
    {
                hasil = done[i];
                theRealHasil = hasil / allProgress;

                bar[i].transform.localScale = new Vector3(1, theRealHasil, 1);

                dayAndProgress[i].text = "" + (i + 1) + "\n" + (theRealHasil * 100).ToString("F0") + "%";
    }



}
