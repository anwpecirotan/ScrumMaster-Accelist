﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
public class MoneyDecreasedPanel : MonoBehaviour
{
    public Database db;
    public TextMeshProUGUI moneyOnText;
    public Button oke;
    public Button skipButton;

    

    float countDown;
    float actualCountDown;
    float moneyText;
    bool start;
    float timer = 0;
    float timerLength = 0.00001f;
    // Start is called before the first frame update
    void Start()

    {
        db = FindObjectOfType<Database>();
        countDown =  1;
        actualCountDown = 0;
        moneyOnText.text = (db.mirrorProjectCash) + " $";
       
        // moneyText = db.projectCash;
    }

    // Update is called once per frame
    void Update()
    {


        //actualCountDown += Time.deltaTime;
        //if (actualCountDown >= countDown)
        //{
        //for(int i = db.mirrorProjectCash; i>=db.projectCash; i--)
        //{
        //    moneyOnText.text = (i) + " $";

        //}

        if (db.mirrorProjectCash >= db.projectCash)
            {
                timer += Time.deltaTime;
                if (timer >= timerLength)
                {
                    db.mirrorProjectCash -= 1;
                    moneyOnText.text = db.mirrorProjectCash + " $";
                    timer = 0;
                    //timerLength += 0.0005f;
                }

            }

          if (db.mirrorProjectCash<= db.projectCash)
                {
                    db.mirrorProjectCash = db.projectCash;
                    moneyOnText.text = (db.mirrorProjectCash) + " $";
                    oke.gameObject.SetActive(true);
                }



        //    actualCountDown = 0;
        //}
    }

    public void closeThisPanel()
    {
        oke.gameObject.SetActive(false);
        skipButton.gameObject.SetActive(true);
        
        gameObject.SetActive(false);
    }

    public void skipTheProgress()
    {
        db.mirrorProjectCash = db.projectCash;
        skipButton.gameObject.SetActive(false);
    }
}
