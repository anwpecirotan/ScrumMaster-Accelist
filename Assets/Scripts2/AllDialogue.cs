﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllDialogue : MonoBehaviour

    
{
    [TextArea(2,10)]
    public string [] problemDescription;
    [TextArea(2, 10)]
    public string [] doButton;
    [TextArea(2, 10)]
    public string [] dontButton;
    [TextArea(2, 10)]
    public string [] cosDoButton;
    [TextArea(2, 10)]
    public string [] cosDontButton;

    public int[] doMoneyValues;
    public int[] doProgressValues;
    public int[] doProductivityValues;
    public int[] doObstacleProofValues;

    public int[] dontMoneyValues;
    public int[] dontProgressValues;
    public int[] dontProductivityValues;
    public int[] dontObstacleProofValues;
    // Start is called before the first frame update
    void Start()
    {



        problemDescription[0] = "Chief, this task require a specific item to finish, i recommend you to buy the Item or we must use another method from zero";
        doButton[0] = "Buy the Item";
        dontButton[0] = "Do Nothing";
        cosDoButton[0] = "Money Spend" + doMoneyValues[0] + "$, Progress +" + doProgressValues[0] + "Hours Worth";
        cosDontButton[0] = "progress -" + dontProgressValues[0];
        //============================================
        problemDescription[1] = "this task is harder than i expected, i can do it myself but it will take a while, i recommend you to Hire a freelancer.";
        doButton[1] = "Hire Freelancer ";
        dontButton[1] = "Ignore ";
        cosDoButton[1] = doMoneyValues[1] + "$ Money Spended";
        cosDontButton[1] = "Employee Productivity -" + dontProductivityValues[1] + "%";
        //============================================
        problemDescription[2] = "some of our progress resetted because the Blackout, i recommend you to rent a cloud services so this problem wont happen again";
        doButton[2] = "Rent a Cloud Services";
        dontButton[2] = "Ignore";
        cosDoButton[2] = doMoneyValues[2] + "$ Money Spend, Progress turn into 0%";
        cosDontButton[2] = "Progress still turn into 0% but this problem will come again";
        //============================================

        problemDescription[3] = " Chief, one of our member is facing a misscom, it slow our progress";
        doButton[3] = "Do a Meeting";
        dontButton[3] = "Ignore";
        cosDoButton[3] = doMoneyValues[3] + "$ Money Spend";
        cosDontButton[3] = "Employee Productivity -" + dontProductivityValues[3] + "%";
        //============================================
        problemDescription[4] = "We Need to create a specific Database chief, some task required to do it";
        doButton[4] = "Build a Database";
        dontButton[4] = "Ignore";
        cosDoButton[4] = doMoneyValues[4] + "$ Money Spend";
        cosDontButton[4] = "progress -" + dontProgressValues[4] + "Hours";
        //============================================
        problemDescription[5] = ". . . .       The Worker seems Absent";
        doButton[5] = "Call Him ";
        dontButton[5] = "Ignore ";
        cosDoButton[5] = "Progress" + doProgressValues[5] + " Hours";
        cosDontButton[5] = "He comes but with bad mood Productivity -" + dontProductivityValues[5] + "%";
        //============================================
        problemDescription[6] = "Sorry chief, there is some problem dragged me back in Home, my works may slowdown";
        doButton[6] = "No Excuse Go Work now";
        dontButton[6] = "Listen to his problem";
        cosDoButton[6] = "His Productivity fall into" + doProductivityValues[6] + "% during for 1 task";
        cosDontButton[6] = "Progress -" + dontProgressValues[6] + "Hours";
        //============================================
        //problemDescription[7] = "Some of our data got stolen by a cat because the USB stick seems smells like fish, damn Bob, he always eat while work";
        //doButton[7] = "Send a Team to retrieve the USB Stick ";
        //dontButton[7] = "Ignore";
        //cosDoButton[7] = doMoneyValues[7] + "$ Money Spend";
        //cosDontButton[7] = "Progress of task reset to 0%";
        ////============================================
        //problemDescription[8] = "Bob spills the tea to his Computer, now all of his data is gone";
        //doButton[8] = "Ban Bob for Eating while work";
        //dontButton[8] = "Ignore";
        //cosDoButton[8] = "this problem wont happen again, Progress = 0 %";
        //cosDontButton[8] = "this problem will happen again, Progress = 0 %";
        ////============================================
        //problemDescription[9] = "Bob dies due starvation, because he can't eat during work";
        //doButton[9] = "Press F to pay respect";
        //dontButton[9] = "Press F to pay respect";
        //cosDoButton[9] = "He appears as Force Ghost, he works normally now";
        //cosDontButton[9] = "He appears as Force Ghost, he works normally now";
        ////==========================================
        problemDescription[7] = "The Worker seems unmotivated for the Overtime shift tonight, what do you do chief?";
        doButton[7] = "Give him a Reward";
        dontButton[7] = "Ignore";
        cosDoButton[7] = "This Staff wont have any Obstacle for " + doObstacleProofValues[7]+ " days straight, You Pay " + doMoneyValues[7] + "$";
        cosDontButton[7] = "The Staff dont do any work for 1 day";
        //===========================================================
        problemDescription[8] = "We have unsolved Bug that should have been Discussed";
        doButton[8] = "Lets gather a meeting";
        dontButton[8] = "Ignore";
        cosDoButton[8] = "All Progress paused for 1 day";
        cosDontButton[8] = "this Task wont progressed untill it solved";
        //===========================================================
        problemDescription[9] = "This Worker lost its Data, we must find it or the Progress will lost forever";
        doButton[9] = "Find it";
        dontButton[9] = "Ignore";
        cosDoButton[9] = "All Progress paused for 1 day to retrieve lost data";
        cosDontButton[9] = "The Progress Lost forever";
        //===========================================================
  

        ///

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
