﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DayOverCon : MonoBehaviour
{
    public Text sprint;
    public Text day;

    public int sprintIdx;
    public int dayIdx;

    public 
    // Start is called before the first frame update
    void Start()
    {
        sprint.text = "Sprint " + (sprintIdx + 1);
        day.text = "Day " + (dayIdx + 1) + "is Over";
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GoToBurnDownCart()
    {
        //Scenemanager here
    }

    public void NextDay()
    {
        //Scenemanager here
    }
}
