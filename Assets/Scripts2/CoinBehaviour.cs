﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class x  : MonoBehaviour
{
    public Text taskIndexText;
    public Image workerPhotoText;
    public Text workerIndexText;
    public Text percentageCompletionText;
    public Text statusText;
    public Slider progress;
    public Slider progressIncreased;

    public Button solveButton;

    public int idxTask;
    public int idxWorker;
    public int idxProblem;

    float percentage;
    public string status;
    public float totalManHour;
    public float totalCleared;
    public float totalAlreadyCleared;

    public bool isProblem;
    public bool isProgress;
    public GameObject frameProgress;
    // public int idxProblem;

    public Database db;


    // Start is called before the first frame update
    void Start()
    {
        
        db = FindObjectOfType<Database>();
        //slider = currentproggrss/allprogress
        //idxTask = //ambil Indx dari Backlog
        //idxWorker = //ambil Indx dari Worker
        //percentage = //ambil percentage dari Backlog
        //statys = //ambil Indx dari Backlog
        //totalManHour = 200;
        //totalCleared = 80;
        //totalAlreadyCleared = 120;


        //========================================
//        print("aku hidup lagi");
//        if(isProgress == false)
//        {
//            progressIncreased.gameObject.SetActive(false);
//            totalAlreadyCleared=totalCleared;
//}
//        if(isProblem == false)
//        {
//            solveButton.gameObject.SetActive(false);
//        }

//        progress.maxValue = totalManHour;
//        progress.value = totalCleared;

//        progressIncreased.value = totalAlreadyCleared;
//        progressIncreased.maxValue = totalManHour;
    

//        taskIndexText.text = "Task " + idxTask;
//        //gatau cara ubah image;
//        workerIndexText.text = "Worker " + idxWorker;
//      //  percentageCompletionText.text = "[" + percentage + "%] Completion : ";
//        statusText.text = "Status : " + status;
    }

    // Update is called once per frame
    void Update()
    {
        print(totalCleared);
        if (totalCleared <= totalAlreadyCleared)
        {
            totalCleared += Time.deltaTime * 1000;
            if (totalCleared > totalAlreadyCleared)
            {
                totalCleared = totalAlreadyCleared;
            }
            percentage = (totalCleared/ totalManHour) * 100;
            progressIncreased.value = totalCleared;
            percentageCompletionText.text = "[" + percentage.ToString("F1") + "%] Completion : ";
            progress.value = totalCleared;
        }
        //percentage = (totalCleared / totalManHour) * 100;


        if (isProgress == false)
        {
            progressIncreased.gameObject.SetActive(false);
            totalCleared = totalAlreadyCleared;
        }
        if (isProblem == false)
        {
            solveButton.gameObject.SetActive(false);
        }

        progress.maxValue = totalManHour;
        progress.value = totalAlreadyCleared;

        progressIncreased.value = totalAlreadyCleared;
        progressIncreased.maxValue = totalManHour;


        taskIndexText.text = "Task " + idxTask;
        //gatau cara ubah image;
        workerIndexText.text = "Worker " + idxWorker;
        //  percentageCompletionText.text = "[" + percentage + "%] Completion : ";
        statusText.text = "Status : " + status;
    }

   public void solveProblem()
    {
        //load Scene berdasarkan index Problem
    }

    public void Close()
    {
        print("trpanggil");
        frameProgress.SetActive(false);
        db.isViewing = false;
        totalCleared = 0;
        totalManHour = 0;
        totalAlreadyCleared = 0;
        status = "Done or Not Worked yet";
        isProblem = false;
        isProgress = false;
        progress.value = 0;

    }
}
