﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SolvingProblem : MonoBehaviour
{
    public Database db;
    public AllDialogue dialogue;
    public ProgressionPanel propan;
    public Text idxText;
    public Text problemDescription;
    public Text doCon;
    public Text dontCon;

    public Text doButton;
    public Text dontButton;
    public float[] staffProductivity;
    public GameObject close;


    public string description, doConText, dontConText, doButtonText, dontButtonText;
    public int taskIdx;
    public int problemIdx;

    // Start is called before the first frame update
    void Start()
    {
       


        
    }

    // Update is called once per frame
    void Update()
    {
         db = FindObjectOfType<Database>();
        dialogue = FindObjectOfType<AllDialogue>();
        propan = FindObjectOfType<ProgressionPanel>();
        
        staffProductivity = new float[db.staffCount];
        idxText.text = "Task " + taskIdx;
        for (int i = 0; i < db.staffCount; i++)
        {
            staffProductivity[i] = 1;
        }
        db.staffProductivity = staffProductivity;
        problemDescription.text = description;

        doCon.text = doConText;
        dontCon.text = dontConText;
   
        doButton.text = doButtonText;
        dontButton.text = dontButtonText;
    }

    public void Closed()
    {
        
        gameObject.SetActive(false);
        db.isViewing = false;
    }

    public void Consequence(int money, int progress, float productivity, int obstacleProof)
    {
        
        db.projectCash += money;
        db.currentBackLog[taskIdx] += progress;
        if (db.currentBackLog[taskIdx] > db.mirrorBackLog[taskIdx])
        {
            db.currentBackLog[taskIdx] = db.mirrorBackLog[taskIdx];
        }
        db.staffProductivity[db.assignedBackLog[taskIdx]-1] *= productivity/100;
        db.obstacleProof[db.assignedBackLog[taskIdx]-1] = obstacleProof;

        db.notSolved[taskIdx] = false;
        db.validateCounter--;
        gameObject.SetActive(false);
       
        propan.Close();
        Closed();
        //close.SetActive(false);
    }


    public void DoCon()
    {
        
        Consequence(dialogue.doMoneyValues[problemIdx], dialogue.doProgressValues[problemIdx], dialogue.doProductivityValues[problemIdx],dialogue.doObstacleProofValues[problemIdx]);
    }

    public void DontCon()
    {
        Consequence(dialogue.dontMoneyValues[problemIdx], dialogue.dontProgressValues[problemIdx], dialogue.dontProductivityValues[problemIdx], dialogue.dontObstacleProofValues[problemIdx]);
    }
}
