﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DaySummaryPanel: MonoBehaviour
{
    public Button showProgress;
    public Button burnDownChart;
    public Button nextDay;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ButtonShowProgress()
    {
        showProgress.onClick.Invoke();
    }
}
