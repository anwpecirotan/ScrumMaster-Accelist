﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class ProgressionPanel : MonoBehaviour
{
    public Text taskIndexText;
    public Image workerPhoto;
    public Image workerBG;
    public Text workerIndexText;
    public Text percentageCompletionText;
    public Text statusText;
    public Slider progress;
    public Slider progressIncreased;

    public Button solveButton;
    public Button nextProgressButton;
    public Button showProgressButton;

    public int idxTask;
    public int idxWorker;
    public int idxProblem;

    float percentage;
    public string status;
    public float totalManHour;
    public float totalCleared;
    public float totalAlreadyCleared;
    

    public bool isProblem;
    public bool isProgress;
    public GameObject frameProgress;
    public GameObject frameProblem;
    public GameObject moneyDecreased;

    public GameObject[] resetObjects;

    public Button buttonClose;

    public Database db;
    public SolvingProblem solve;
    public AllDialogue dialogue;
    public ScrumTeam st;
    public ScrumBoard sb;

    void Start()
    {
        db = FindObjectOfType<Database>();
        st = FindObjectOfType<ScrumTeam>();
        sb = FindObjectOfType<ScrumBoard>();
        dialogue = FindObjectOfType<AllDialogue>();
        
    }

    void Update()
    {
        
        if (totalCleared <= totalAlreadyCleared)
        {
            totalCleared += Time.deltaTime*15;
            if(totalCleared > totalAlreadyCleared)
            {
                totalCleared = totalAlreadyCleared;
            }
            percentage = (totalCleared / totalManHour) * 100;
            progressIncreased.value = totalAlreadyCleared;
            percentageCompletionText.text = "[" + percentage.ToString("F1") + "%] Completion : ";
            progress.value = totalCleared;
            
        }
        setPanelValues();
        
    }

    public void setPanelValues() {
        //percentage = (totalCleared / totalManHour) * 100;
        if (isProgress == true)
        {
            nextProgressButton.gameObject.SetActive(true);
            progressIncreased.gameObject.SetActive(true);
            buttonClose.gameObject.SetActive(false);
            //totalAlreadyCleared = totalCleared;
        }
        else
        {
            buttonClose.gameObject.SetActive(true);
        }

        if (db.notSolved[idxTask])
        {
            solveButton.gameObject.SetActive(true);
        }

        if(db.isDone[idxTask] == true){
            status = "Done";
        }
        progress.maxValue = totalManHour;
        progress.value = totalCleared;

        progressIncreased.value = totalAlreadyCleared;
        progressIncreased.maxValue = totalManHour;
        
      //  Debug.Log(idxWorker);

        // <----- problem appears here when a backlog is cleared, maybe idxworker value is 0? possible fix by adding an if statement
        if (idxWorker != 0)
        {
            //sprite and bg change on panel
            //set bg color
            workerBG.color = new Color(st.colorValues[idxWorker - 1].x, st.colorValues[idxWorker - 1].y, st.colorValues[idxWorker - 1].z);

            if (db.mirrorStaffTeam[idxWorker - 1] == 5)
            {
                //set sprite
                workerPhoto.sprite = st.sprites[0];
            }
            else if (db.mirrorStaffTeam[idxWorker - 1] == 10)
            {
                //set sprite
                workerPhoto.sprite = st.sprites[1];
            }
            else if (db.mirrorStaffTeam[idxWorker - 1] == 15)
            {
                //set sprite
                workerPhoto.sprite = st.sprites[2];
            }
            else if (db.mirrorStaffTeam[idxWorker - 1] == 20)
            {
                //set sprite
                workerPhoto.sprite = st.sprites[3];
            }
        }
        else {
            workerPhoto.sprite = null;
        }
        // set text
        taskIndexText.text = "Task " + idxTask;
        workerIndexText.text = "Worker " + idxWorker;
        //  percentageCompletionText.text = "[" + percentage + "%] Completion : ";
        statusText.text = "Status : " + status;
    }

    public void solveProblem()
    {
        //load Scene berdasarkan index Problem        
        frameProblem.SetActive(true);
        
        //set text on problem panel
        solve = FindObjectOfType<SolvingProblem>();
        solve.taskIdx= idxTask;
        solve.problemIdx = idxProblem;
        solve.description = dialogue.problemDescription[idxProblem];
        solve.doConText = dialogue.cosDoButton[idxProblem];
        solve.dontConText = dialogue.cosDontButton[idxProblem];
        solve.doButtonText = dialogue.doButton[idxProblem];
        solve.dontButtonText = dialogue.dontButton[idxProblem];

        
    }

    public void ResetVariables() {
        db.isViewing = false;
        db.doneScan = false;
        db.reduced = false;
        for(int i = 0; i < db.staffProductivity.Length; i++){
            db.staffProductivity[i] = 1;
        }
    }

    public void ResetMarkers() {
        for (int i = 0; i < db.currentBackLog.Length; i++ )
        {
            if(db.currentBackLog[i] <= 0){
                //reduce staff task count here
                if (db.assignedBackLog[i] != 0) // ----> possible solution to the problem below!
                { 
                    db.staffTasks[db.assignedBackLog[i] - 1]--; // <---- problem here as there is a 0 value in ABL?
                    //also set ABL to 0 
                    db.assignedBackLog[i] = 0;
                    //set marker to done here
                    sb.markerImage[i].sprite = sb.markerSprites[2];
                    sb.markerImage[i].gameObject.tag = "doneMarker";
                    db.notSolved[i] = false;
                    db.isDone[i] = true;
                }
            }
        }
    }

    public void NewDayProtocol() {
        ResetMarkers();
        sb.j = 0;
        sb.ActivateMarkers();
        ResetVariables();
        //set all warning texts and panel off
        for (int i = 0; i < resetObjects.Length; i++ )
        {
            resetObjects[i].SetActive(false);
        }
    }

    public void NextProgress()
    {
        //if(db.progressCounter >= db.CurrentPostwMark.Length-1)
        if (db == null)
        {
            db = FindObjectOfType<Database>();
        }
        if (db.progressCounter >= db.CPWMC-1)
            {

            db.progressCounter = 0;
            //isProgress = false;
            Close();
            moneyDecreased.SetActive(true);
            //moved to buttons
            //NewDayProtocol();
           
        }
        else
        {
            
        ResetVariable();
        gameObject.SetActive(true);
        db.progressCounter++;
        print(db.progressCounter);
       
        isProgress = true;
        PrintProgress();
       
        
        }  
    }

    public void ResetVariable()
    {
        totalAlreadyCleared = 0;
        totalCleared = 0;
        isProgress = false;
        isProblem = false;
        status = " ";
    }

    public void Close()
    {   
        if(solveButton.IsActive())
        {
            solveButton.gameObject.SetActive(false);
        }
        if(nextProgressButton.IsActive())
        { 
            nextProgressButton.gameObject.SetActive(false);
        }
        totalAlreadyCleared = 0;
        totalCleared = 0;
        isProgress = false;
        isProblem = false;
        status = " ";
        db.isViewing = false;
        progress.value = 0;
        progressIncreased.value = 0;
        frameProgress.SetActive(false);

         
    }

    public void PrintProgress()
    {

        idxTask = db.CurrentPostwMark[db.progressCounter];
        idxWorker = db.assignedBackLog[db.CurrentPostwMark[db.progressCounter]];
        //totalCleared =
        totalAlreadyCleared = db.mirrorBackLog[db.CurrentPostwMark[db.progressCounter]] - db.currentBackLog[db.CurrentPostwMark[db.progressCounter]];
        totalManHour = db.mirrorBackLog[db.CurrentPostwMark[db.progressCounter]];
    }

    public void BurnDownChart()
    {
        resetObjects[0].SetActive(false);
    }

    //public void printNextProgress()
    //{
        
    //    isProgress = true;
    //    PrintProgress();
        
        
    //}
}
