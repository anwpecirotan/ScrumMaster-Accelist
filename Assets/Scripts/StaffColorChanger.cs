﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaffColorChanger : MonoBehaviour
{
    //script used in scene 4 only

    public SpriteRenderer sr_staffColor;
    

    void Start()
    {
        
        if(gameObject.transform.position.x >= -5 && gameObject.transform.position.x <= -4){
            sr_staffColor.color = new Color(1f, 0.8f, 0.82f);
            sr_staffColor.gameObject.tag = "ColorIndex1";
        }
        else if (gameObject.transform.position.x >= -3.5f && gameObject.transform.position.x <= -2.5f)
        {
            sr_staffColor.color = new Color(1f, 0.87f, 0.82f);
            sr_staffColor.gameObject.tag = "ColorIndex2";
        }
        else if (gameObject.transform.position.x >= -2 && gameObject.transform.position.x <= -1)
        {
            sr_staffColor.color = new Color(1f, 1f, 0.82f);
            sr_staffColor.gameObject.tag = "ColorIndex3";
        }
        else if (gameObject.transform.position.x >= -0.5f && gameObject.transform.position.x <= 1.5f)
        {
            sr_staffColor.color = new Color(0.82f, 1f, 0.78f);
            sr_staffColor.gameObject.tag = "ColorIndex4";
        }
        else if (gameObject.transform.position.x >= 2 && gameObject.transform.position.x <= 3)
        {
            sr_staffColor.color = new Color(0.82f, 0.87f, 1f);
            sr_staffColor.gameObject.tag = "ColorIndex5";
        }
        else if (gameObject.transform.position.x >= 3.5f && gameObject.transform.position.x <= 4.5f)
        {
            sr_staffColor.color = new Color(0.58f, 0.28f, 0.8f);
            sr_staffColor.gameObject.tag = "ColorIndex6";
        }
        
    }
}
