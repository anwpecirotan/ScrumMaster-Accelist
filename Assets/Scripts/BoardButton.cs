﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardButton : MonoBehaviour
{
    //public GameObject everything;
    public ScrumBoardAnimator sba;

    void Start()
    {
        sba = FindObjectOfType<ScrumBoardAnimator>();
    }

    void Update()
    {
        
    }

    private void OnMouseDown()
    {
        if (gameObject.tag == "SeeBoard")
        {
            MoveToBoard();
        }
        else if(gameObject.tag == "SeeStaff"){
            MoveToStaff();
        }
    }

    public void MoveToBoard() {
        sba.anm.SetBool("editSCrum", true);
    }

    public void MoveToStaff() {
        sba.anm.SetBool("editSCrum", false);
    }
}
