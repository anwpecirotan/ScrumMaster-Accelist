﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Database : MonoBehaviour
{
    //scene0

    //scene1
    public string projectName;
    public int projectCash;
    public int mirrorProjectCash;
    public int totalManHour;
    public int[] sprints;
    public int[] mirrorSprints;
    public int currentSprint;
    public int totalSprints;
    public int totalBacklogs;
    public int doneMarker;
    //scene2
    public bool firstTime;
    public int[] currentBackLog;
    public int[] mirrorBackLog;

    //scene3
    public int staffCount;
    public int[] currentStaffTeam;
    public int[] mirrorStaffTeam;

    //scene4
    public int[] assignedBackLog;

    //scene5
    public int[] obstacleProof;
    public int[] staffTasks;
    public int[] rollResult;
    public bool impure;
    public bool doneScan;
    public bool isViewing = false;
    public int[] CurrentPostwMark;
    public bool reduced = false;
    public bool doneReduce = false;
    public bool[] notSolved;
    public int validateCounter = 0;
    public float[] staffProductivity;
    public int dayCounter;
    public bool newDay = false;
    //public int sprintCounter;
    public bool[] isDone;
    //CPWMC stands for CurrentPostWMarkCounter
    public int CPWMC;
    public bool talkedWithAll = false;
    public bool[] talkedWith;

    //Scene EndDay
    public int progressCounter;
    public bool[] willHappenAgain;

    public int allDaySpent;
    
    

    public static Database instance;

    void Awake() {
        if (instance == null)
        {
            instance = this;
        }
        else {
            Destroy(gameObject);
            //return;
        }
        DontDestroyOnLoad(gameObject);
    }

    //call on new sprint
    public void SoftReset()
    {
        firstTime = true;
        isViewing = false;
        reduced = false;
        validateCounter = 0;
        newDay = false;
        doneReduce = false;
        isDone = new bool[20];
        totalBacklogs = 0;
        dayCounter = 1;
        progressCounter = 0;
        doneMarker = 0;
        for (int i = 0; i < talkedWith.Length; i++ )
        {
            talkedWith[i] = false;
        }
        talkedWithAll = false;
        
    }

    //call on new project
    public void HardReset() 
    {
        SoftReset();
        allDaySpent = 0;
        totalSprints = 0;
    }
}
