﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuteBar : MonoBehaviour
{

    public GameObject muteBar;
    public AudioControls ac;

    void Start()
    {
        ac = FindObjectOfType<AudioControls>();
        muteBar.SetActive(false);
        PlayerPrefs.SetInt("mute", 0);
    }

    void Update()
    {
        if(PlayerPrefs.GetInt("mute") == 1)
        {
            //muted
            muteBar.SetActive(true);
        }
        else if (PlayerPrefs.GetInt("mute") == 0)
        {
            //not muted
            muteBar.SetActive(false);
        }
    }

    public void mute() {
        ac.mute();
    }
}
