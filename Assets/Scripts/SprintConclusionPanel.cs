﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SprintConclusionPanel : MonoBehaviour
{
    public Database db;
    public Text sprintEnded;
    public string sprintEndedText;

    public Text spentDay;
    public string spentDayText;

    public Text moneyLeft;
    public string moneyLeftText;


    // Start is called before the first frame update
    void Start()
    {
        db = FindObjectOfType<Database>();

        sprintEndedText = ""+db.currentSprint;
        spentDayText = "" + db.dayCounter;
        moneyLeftText = "" + db.projectCash;

        sprintEnded.text = "Sprint " + sprintEndedText + "\n" + "[ "+sprintEndedText+" / "+(db.totalSprints)+" ]";
        spentDay.text = "You spent " + spentDayText + " Days to this Sprint,";
        moneyLeft.text = "You have " + moneyLeftText + " $ left";

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void nextSprint()
    {
        db.currentSprint++;
        db.SoftReset();
        SceneManager.LoadScene(2);
    }
}
