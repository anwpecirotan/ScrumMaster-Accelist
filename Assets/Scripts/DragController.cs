﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragController : MonoBehaviour
{
    public AudioControls ac;
    public CircleCollider2D biggerCollider;
    public CircleCollider2D smallerCollider;

    private float deltaX, deltaY;
    private Vector2 mousePosition;
    private Vector2 initialPosition;

    //Ukuran No
    public Vector3 startSize;
    public Vector3 clickedSize;

    //Target dimana objek harus diletakkan
    public GameObject targetObject;

    //Apakah objek sudah di posisi target
    public bool onTarget;

    //Apakah objek sudah terkunci di posisi target
    public bool locked;

    //replacement conditions
    public bool mouseHold;
    public bool replace;

    public bool firstGen = true;

    public GameObject selfPrefab;
    public GameObject collidedStaff;
    public GameObject secondCollidedStaff;
    public SpriteRenderer sr;
    public SpriteRenderer sr2;
    public SpriteRenderer sr3;

    void Start()
    {
        ac = FindObjectOfType<AudioControls>();
        initialPosition = transform.position;
        if (firstGen == true)
        {
            firstGen = false;
            Instantiate(selfPrefab, initialPosition, Quaternion.identity);
            Destroy(gameObject);
        }
        startSize = gameObject.transform.localScale;
        startSize = new Vector3(1, 1, 1);
        clickedSize = startSize * 1.2f;
        locked = false;
        onTarget = false;
        sr = gameObject.GetComponent<SpriteRenderer>();
        biggerCollider.enabled = true;
        smallerCollider.enabled = false;
    }

    void Update()
    {

    }

    private void OnMouseDown()
    {
        mouseHold = true;
        if (locked == false)
        {
            sr.sortingOrder += 3;
            sr2.sortingOrder += 3;
            sr3.sortingOrder += 3;
            deltaX = Camera.main.ScreenToWorldPoint(Input.mousePosition).x - transform.position.x;
            deltaY = Camera.main.ScreenToWorldPoint(Input.mousePosition).y - transform.position.y;
            transform.localScale = clickedSize;
        }
        if (locked == true)
        {
            Destroy(gameObject);
        }
    }

    private void OnMouseDrag()
    {
        //if (locked == false) {
        mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = new Vector2(mousePosition.x - deltaX, mousePosition.y - deltaY);
        //}
    }

    private void OnMouseUp()
    {
        if (replace == true)
        {
            Destroy(collidedStaff);
            replace = false;
        }
        //hover over other objects
        mouseHold = false;
        sr.sortingOrder -= 3;
        sr2.sortingOrder -= 3;
        sr3.sortingOrder -= 3;
        if (!onTarget)
        {
            //back to initial pos
            transform.position = new Vector2(initialPosition.x, initialPosition.y);
            transform.localScale = startSize;
        }
        else
        {
            ac.PlaySFX(0, ac.lo_vol);
            //new position
            transform.position = targetObject.transform.position;
            transform.localScale = startSize;
            locked = true;
            biggerCollider.enabled = false;
            smallerCollider.enabled = true;
            //make new one
            Instantiate(selfPrefab, initialPosition, Quaternion.identity);
        }
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        //Debug.Log("hit!");
        if (coll.tag == targetObject.tag)
        {
            onTarget = true;
            targetObject = coll.gameObject;
        }
    }

    void OnTriggerStay2D(Collider2D coll)
    {
        //replace staff
        if (coll.tag == "staff" && onTarget == true
            || coll.tag == "staff2" && onTarget == true
            || coll.tag == "staff3" && onTarget == true
            || coll.tag == "staff4" && onTarget == true)
        {
            if (locked == false && mouseHold == true)
            {
                if (collidedStaff == null)
                {
                    collidedStaff = coll.gameObject;
                }
                replace = true;
            }
        }
    }

    void OnTriggerExit2D(Collider2D coll)
    {
        if (coll.tag == targetObject.tag)
        {
            onTarget = false;
            collidedStaff = null;
        }

        if (coll.tag == "staff" ||
            coll.tag == "staff2" ||
            coll.tag == "staff3" ||
            coll.tag == "staff4")
        {
            collidedStaff = null;
        }
    }
}
