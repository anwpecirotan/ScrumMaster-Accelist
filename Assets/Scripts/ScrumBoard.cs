﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ScrumBoard : MonoBehaviour
{

    public SpriteRenderer[] postImage;
    public SpriteRenderer[] markerImage;    
    public Sprite[] postSprites;
    public Sprite[] markerSprites;
    public Database db;
    public int[] staffTasks;
    public int[] diceRoll;
    public int[] CurrentPostwMark;
    public int j;

    void Start()
    {
        j = 0;
        db = FindObjectOfType<Database>();
        CurrentPostwMark = new int[db.staffCount];
        diceRoll = new int[db.staffCount]; 
        CountStaffTasks();
        SetPostValues();
        ActivateMarkers();
    }


    void Update()
    {
        
    }

    public void CountStaffTasks() {
        staffTasks = new int[6];
        for (int i = 0; i < db.staffCount; i++ ) 
        {
            for (int j = 0; j < db.assignedBackLog.Length; j++ )
            {
                if(db.assignedBackLog[j] - 1 == i){
                    staffTasks[i]++;
                }
            }
        }
        db.staffTasks = staffTasks;
    }

    public void ActivateMarkers() {
        //random marker type
        for (int x = 0; x < db.staffCount; x++)
        {
            if (db.dayCounter <= 1 || db.obstacleProof[x] > 0)
            {
                diceRoll[x] = Random.Range(11, 30);

                if (db.obstacleProof[x] > 0)
                {
                    db.obstacleProof[x]--;
                }
            }
            else
            {
                diceRoll[x] = Random.Range(0, 30);
                if (diceRoll[x] >= 0 && diceRoll[x] < 10)
                {
                    db.obstacleProof[x] = 1;
                }
            }    
        } db.rollResult = diceRoll;

        //CSIC stands for Current Staff Index Counter
        int CSIC = 1;

        //activate markers
        do{
            for(int i = 0; i < db.assignedBackLog.Length; i++){
                if (db.assignedBackLog[i] == CSIC)
                {
                    if (db.mirrorBackLog[i] == db.currentBackLog[i])
                    {

                        db.rollResult[j] = 29;
                    }
                    //markerImage[i].gameObject.SetActive(true);
                    //set marker sprite
                    if (db.rollResult[j] >= 0 && db.rollResult[j] <= 9)
                    {
                        markerImage[i].sprite = markerSprites[1];
                        CurrentPostwMark[j] = i;
                        markerImage[i].color = new Color(1,1,1);
                        markerImage[i].gameObject.tag = "obsMarker";
                        db.validateCounter++;
                        db.notSolved[i] = true;
                        j++;
                        
                        //insert new array controlling problem dialogues according to roll value into database here!
                    }
                    else if (db.rollResult[j] > 9 && db.rollResult[j] <= 29)
                    {
                        markerImage[i].sprite = markerSprites[0];
                        CurrentPostwMark[j] = i;
                        markerImage[i].color = new Color(1, 1, 1);
                        markerImage[i].gameObject.tag = "proMarker";
                        db.notSolved[i] = false;
                        j++;

                    }       
                     break;               
                }
            }
            CSIC++;
        }while(CSIC < db.staffCount+1);
        db.CurrentPostwMark = CurrentPostwMark;
    }

    public void SetPostValues() {
        for (int i = 0; i < 20; i++)
        {
            if(db.currentBackLog[i] == 50)
            {
                postImage[i].sprite = postSprites[0];
                postImage[i].gameObject.tag = "post50";
            }
            else if (db.currentBackLog[i] == 100)
            {
                postImage[i].sprite = postSprites[1];
                postImage[i].gameObject.tag = "post100";
            }
            else if (db.currentBackLog[i] == 150)
            {
                postImage[i].sprite = postSprites[2];
                postImage[i].gameObject.tag = "post150";
            }
            else if (db.currentBackLog[i] == 200)
            {
                postImage[i].sprite = postSprites[3];
                postImage[i].gameObject.tag = "post200";
            }
            else if (db.currentBackLog[i] == 0)
            {
                postImage[i].gameObject.SetActive(false);
            }
        }
    }
}


