﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorkButton : MonoBehaviour
{
    public Database db;
    public ScrumScan ss1;
    public ScrumScan ss2;
    public ScrumScan ss3;
    public ScrumScan ss4;
    public bool lagiLoading;
    public float countDown;
    public float realCountDown;
    public GameObject warningText;
    public GameObject sprintConclusion;
    public BoardButton bb;
    public Chattable ct;

    void Start()
    {
        db = FindObjectOfType<Database>();
        bb = FindObjectOfType<BoardButton>();
        ct = FindObjectOfType<Chattable>();

        countDown = 0.3f;
        realCountDown = 0;
        lagiLoading = false;
    }

    void Update()
    {
        if (lagiLoading == true )
        {
            realCountDown += Time.deltaTime;
            if (realCountDown >= countDown)
            {
                lagiLoading = false;
                realCountDown = 0;
            }
        }
    }

    private void OnMouseDown(){
        //scan now
        if (lagiLoading == false && db.validateCounter <= 0)
        {
            realCountDown = 0;
            lagiLoading = true;
            db.CPWMC = 0;
            ss1.scanBoard();
            ss2.scanBoard();
            ss3.scanBoard();
            ss4.scanBoard();
        }

        if(db.validateCounter != 0){
            warningText.SetActive(true);
        }

       
    }
}
