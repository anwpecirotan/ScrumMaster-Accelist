﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class ScrumScan : MonoBehaviour
{
    int counter;
    int doneMarker;
    public Database db;
    public Chattable ct;
    public ProgressionPanel progPanel;
    public DaySummaryPanel sumPanel;
    
    public string targetTag;
    public bool impure;
    public bool startScan;
    public bool init;
    public Vector3 initialPos;
    public GameObject daySummaryPanel;
    public GameObject burnDownChart;
    public GameObject warningText;
    public GameObject progressPanel;
    public GameObject sprintConclusion;


    void Start()
    {
        db = FindObjectOfType<Database>();
        ct = FindObjectOfType<Chattable>();
        
        
        db.doneScan = false;
        init = false;
        //reduced = false;
        startScan = false;
        initialPos = gameObject.transform.position;
    }

    void Update()
    {
        
        if (startScan == true && gameObject.transform.position.y > -11) {

            transform.position += transform.up * Time.deltaTime * -100;
            if (db.impure == true && gameObject.transform.position.y < -11)
            {
                //go back
                startScan = false;
                gameObject.transform.position = initialPos;
                warningText.SetActive(true);
            }

            if (db.impure == false && gameObject.transform.position.y <= -3)
            {

                daySummaryPanel.SetActive(true);
                burnDownChart.SetActive(true);
               

                db.doneScan = true;
                startScan = false;
                
                gameObject.transform.position = initialPos;
           
                    if (db.reduced == false)
                    {

                        ct.ResetDay();
                        db.reduced = true;
                        db.newDay = true;

                    }
                if (db.doneReduce == true)
                    
                    {
                        for (int k = 0; k < db.staffCount; k++)
                        {
                            if (db.currentStaffTeam[k] > 0 && db.staffTasks[k] <= 0)
                            {
                                for (int l = 0; l < db.staffCount; l++)
                                {
                                    if (db.staffTasks[l] > 0)
                                    {
                                        db.currentStaffTeam[l] += ((db.currentStaffTeam[k] + 1) / db.staffCount);
                                    }
                                }
                            db.currentStaffTeam[k] = 0;
                            }
                        }
                    EndDayProtocol();
                    }
                
            }
        }
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag != targetTag)
        {
            if (
            col.tag != "post50" &&
            col.tag != "post100" &&
            col.tag != "post150" &&
            col.tag != "post200" &&
            col.tag != "SeeStaff" )
            {
                db.impure = true;
                counter++;
            }
        }
        if (col.tag == "obsMarker" || col.tag == "proMarker") {
            db.CPWMC++;
        }

        if(targetTag == "doneMarker")
        {
         if(col.tag == "doneMarker")
                {
                    db.doneMarker++;
                    print("Done Marker = " + doneMarker);
                    if (db.sprints[db.currentSprint] <= 0 && db.doneMarker == db.totalBacklogs)
                    {
                        sprintConclusion.SetActive(true);
                    //new sprint protocol
                    ct.ResetDay();
                    db.allDaySpent += db.dayCounter;

                    }
                }
        }
       
    }   

    public void scanBoard() {   
        counter = 0; 
        db.impure = false;
        initialPos = gameObject.transform.position;
        startScan = true;
        db.doneMarker = 0;
    }

    public void EndDayProtocol()
    {
      
        progressPanel.SetActive(true);
        progPanel = FindObjectOfType<ProgressionPanel>();
        // progPanel.NextProgress();
        progPanel.isProgress = true;
        progPanel.totalAlreadyCleared = db.mirrorBackLog[db.CurrentPostwMark[db.progressCounter]] - db.currentBackLog[db.CurrentPostwMark[db.progressCounter]];
        progPanel.totalManHour = db.mirrorBackLog[db.CurrentPostwMark[db.progressCounter]];
        progPanel.idxTask = db.CurrentPostwMark[db.progressCounter];
        progPanel.idxWorker = db.assignedBackLog[db.CurrentPostwMark[db.progressCounter]];
        
    }

}
