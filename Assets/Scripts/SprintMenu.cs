﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class SprintMenu : MonoBehaviour
{
    //int currentSprint;
    int totalManHour;
    int manHourSprint;
    public int[] productBacklogs;

    public Text[] txtPoint;
    public Text[] index;
    public Image[] frame;
    public GameObject[] staffHolder;
    public GameObject[] backLogContainer;

    public Database db;
    //public GenerateManHour gmh;
    //public Text txtCurrentSprint;
    

    void Start()
    {
        db = FindObjectOfType<Database>();
        if(db.firstTime == true){
            totalManHour = db.sprints[db.currentSprint];
        }
        SprintPlanning();
        if(db.firstTime == true){
            //db.firstTime = false;
            SaveVars();
            
        }
    }

    void Update()
    {

    }

    public void SaveVars() {
        db.currentBackLog = productBacklogs;
        for (int i = 0; i < db.mirrorBackLog.Length; i++ )
        {
            db.mirrorBackLog[i] = db.currentBackLog[i];
        }
    }

    public void SprintPlanning() { //MainHourSprint Code   
        if(db.firstTime == false){
            productBacklogs = db.currentBackLog;
            
            TextConnection();
        }
        if (totalManHour > 0 && db.firstTime == true)
        {
            for (int i = 0; i < 20; i++)
            {
                if (totalManHour >= 200)
                {
                    productBacklogs[i] = Random.Range(1, 5) * 50;
                    totalManHour -= productBacklogs[i];
                }
                else
                {
                    productBacklogs[i] = totalManHour;
                    totalManHour = 0;
                    i = 20;
                }
                db.totalBacklogs++;
            }
            TextConnection();
        }     
    }

    public void TextConnection()
    {
            //text with productBackLogs
            for (int i = 0; i < 20; i++)
            {
                txtPoint[i].text = productBacklogs[i].ToString() + " Hours";
                index[i].text = i + 1 + "";
                if (productBacklogs[i] == 200)
                {
                    //purple
                    frame[i].color = new Color(0.85f, 0, 0.9f);
                    staffHolder[i].tag = "post200";
                }
                else if (productBacklogs[i] == 150)
                {
                    //Blue
                    frame[i].color = new Color(0.77f, 0.88f, 0.81f);
                    staffHolder[i].tag = "post150";
                }
                else if (productBacklogs[i] == 100)
                {
                    //Yellow
                    frame[i].color = new Color(0.99f, 0.99f, 0.58f);
                    staffHolder[i].tag = "post100";
                }
                else if (productBacklogs[i] == 50)
                {
                    //Green
                    frame[i].color = new Color(0.46f, 0.86f, 0.46f);
                    staffHolder[i].tag = "post50";
                }
                else if (productBacklogs[i] == 0)
                {
                    backLogContainer[i].SetActive(false);
                    staffHolder[i].SetActive(false);
                    Destroy(backLogContainer[i]);
                    Destroy(staffHolder[i]);
                }
            }
        
    }
}
