﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ScrumTeam : MonoBehaviour
{
    public Database db;
    public GameObject[] chatBubbles;
    public GameObject[] hitBoxes;
    public SpriteRenderer[] staffImageType;
    public SpriteRenderer[] staffColorIdx;
    public TextMeshPro[] wageText;
    public Sprite[] sprites;
    public Vector3[] colorValues;
    public Vector3 thirdSize;


    void Start()
    {
        db = FindObjectOfType<Database>();
        thirdSize = new Vector3(0.33f,0.33f,0.33f);
        SetTeam();
    }

    void Update()
    {
        
    }

    public void SetTeam() {
        for (int i = 0; i < db.currentStaffTeam.Length; i++)
        {
            
            staffColorIdx[i].color = new Color (colorValues[i].x, colorValues[i].y, colorValues[i].z);
            if (db.currentStaffTeam[i] == 5)
            {
                staffImageType[i].sprite = sprites[0];
                wageText[i].text = db.currentStaffTeam[i] + "$";
            }
            else if (db.currentStaffTeam[i] == 10)
            {
                staffImageType[i].sprite = sprites[1];
                wageText[i].text = db.currentStaffTeam[i] + "$";
            }
            else if (db.currentStaffTeam[i] == 15)
            {
                staffImageType[i].sprite = sprites[2];
                wageText[i].text = db.currentStaffTeam[i] + "$";
            }
            else if (db.currentStaffTeam[i] == 20)
            {
                staffImageType[i].sprite = sprites[3];
                wageText[i].text = db.currentStaffTeam[i] + "$";
            }
            else if (db.currentStaffTeam[i] == 0)
            {
                staffImageType[i].gameObject.SetActive(false);
                staffColorIdx[i].gameObject.SetActive(false);
                wageText[i].gameObject.SetActive(false);
                hitBoxes[i].gameObject.SetActive(false);
                chatBubbles[i].gameObject.SetActive(false);
            }
            staffImageType[i].gameObject.transform.localScale = thirdSize;
        }
    }
}
