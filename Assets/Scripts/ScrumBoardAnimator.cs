﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrumBoardAnimator : MonoBehaviour
{
    public Animator anm;
    private float deltaX;
    private Vector2 mousePosition;

    void Start()
    {
        anm = gameObject.GetComponent<Animator>();
    }

    void Update()
    {
        
        if(gameObject.transform.position.x > -2.75)
        {
            gameObject.transform.position = new Vector3(-2.75f, gameObject.transform.position.y, gameObject.transform.position.z);
        }
        if (gameObject.transform.position.x < -21)
        {
            gameObject.transform.position = new Vector3(-21, gameObject.transform.position.y, gameObject.transform.position.z);
        }
        
    }

    private void OnMouseDown()
    {
        deltaX = Camera.main.ScreenToWorldPoint(Input.mousePosition).x - transform.position.x;
    }

    private void OnMouseDrag()
    {
        if (gameObject.transform.position.x <= -2.75f && gameObject.transform.position.x >= -21)
        {
            mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            transform.position = new Vector3(mousePosition.x - deltaX, gameObject.transform.position.y, 21);
        }
    }
}
