﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GenerateManHour : MonoBehaviour
{
    int totalManHour;
    public int[] sprints;
    int totalSprints;
    int sprintNo;
    int projectCash;
    int projectName;
    int projectNo;
    public Text NamaProject;
    public Text ProjectCash;
    public Text TotalManHour;
    public Text DividedToSprints;
    public Text[] PrintSprint;
    public GameObject panel1;
    public Database db;
    public AudioControls ac;

    void Start()
    {
        totalSprints = 0;
        getProjectName();
        getTotalManHour();
        getSprintHour();
        getProjectCash();
        db = FindObjectOfType<Database>();
        ac = FindObjectOfType<AudioControls>();
        db.firstTime = true;
        ac.PlaySFX(2, ac.lo_vol);
        SaveVars();
    }
    
    void Update()
    {
        TotalManHour.text = totalManHour + " hours to finish";
        getDividedToSprints();
    }

    public void SaveVars()
    {
        db.projectName = "test_Project_Name";
        db.projectCash += projectCash;
        db.mirrorProjectCash = db.projectCash;
        db.totalManHour = totalManHour;
        db.sprints = sprints;
        db.currentSprint = 0;
        db.mirrorSprints = new int[db.sprints.Length];
        for (int i = 0; i < db.sprints.Length; i++)
        {
            db.mirrorSprints[i] = db.sprints[i];
        }
    }

    void getProjectName()
    {
        projectNo += 1;
        projectName = projectNo;
        NamaProject.text = "Project " + projectName;
    }

    void getProjectCash()
    {
        projectCash = Random.Range(15, 30) * 100 + (totalManHour);
        ProjectCash.text = "Project Cash: $" + projectCash;
    }
    
    void getTotalManHour()
    {
        totalManHour = Random.Range(6, 15) * 500;
    }

    void getDividedToSprints()
    {
       // totalSprints = (totalManHour / 1000)+1;
        DividedToSprints.text = "Divided into " + totalSprints + " sprints";
        db.totalSprints = totalSprints;
        for (int i = 0; i < totalSprints; i++)
        {
            PrintSprint[i].text = "Sprint " + (i + 1) + " [" + sprints[i] + " hours]";

            if (sprints[i] == 0)
            {
                PrintSprint[i].text = "";
            }
        }

    }

    void getSprintHour()
    {
        int currentManHour = totalManHour;
        for (int i = 0; i < sprints.Length - 1; i++)
        {
            if (currentManHour > 1500)
            {
                sprints[i] = Random.Range(10, 16) * 100;
                  
                currentManHour -= sprints[i];
            }
            else
            {
                if (currentManHour <= 1500 && currentManHour > 0)
                {
                    sprints[i] = currentManHour;
                    currentManHour = 0;
                    totalSprints = i + 1;
                    i = sprints.Length;
                    
                }              
            }      
        }
    }
}
