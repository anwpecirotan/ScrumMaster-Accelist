﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverDisplay : MonoBehaviour
{
    public Database db;
    public AudioControls ac;
    public TextMeshProUGUI dayText;
    public TextMeshProUGUI cashText;

    void Start()
    {
        db = FindObjectOfType<Database>();
        ac = FindObjectOfType<AudioControls>();
        dayText.text = "Day\n"+ db.dayCounter + " / 30";
        cashText.text = "Project Budget\n" + db.projectCash + " $";
        ac.StopBGM(0);
        ac.StopBGM(1);
        ac.PlaySFX(5,ac.lo_vol);
    }

    public void NewProject() {
        db.projectCash = 0;
        db.SoftReset();
    }

}
