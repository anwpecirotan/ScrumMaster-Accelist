﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class SprintTeam : MonoBehaviour
{
    //a script to generate saved team on scene4
    public Database db;
    public GameObject[] staffs;
    public GameObject staffPrefab1;
    public GameObject staffPrefab2;
    public GameObject staffPrefab3;
    public GameObject staffPrefab4;
    public TextMeshPro[] wageText;

    void Start()
    {
        db = FindObjectOfType<Database>();
        displayStaffTeam();
        
    }

    void Update()
    {
        
    }

    public void displayStaffTeam() {
        for (int i = 0; i < db.currentStaffTeam.Length; i++ )
        {       
            if(db.currentStaffTeam[i] == 5)
            {
                Instantiate(
                    staffPrefab1, 
                    staffs[i].gameObject.transform.position, 
                    Quaternion.identity);
                staffs[i].SetActive(false);
                wageText[i].text = db.currentStaffTeam[i] + "$";
            }
            else if(db.currentStaffTeam[i] == 10)
            {
                Instantiate(
                    staffPrefab2, 
                    staffs[i].gameObject.transform.position, 
                    Quaternion.identity);
                staffs[i].SetActive(false);
                wageText[i].text = db.currentStaffTeam[i] + "$";
            }
            else if (db.currentStaffTeam[i] == 15)
            {
                Instantiate(
                    staffPrefab3, 
                    staffs[i].gameObject.transform.position, 
                    Quaternion.identity);
                staffs[i].SetActive(false);
                wageText[i].text = db.currentStaffTeam[i] + "$";
            }
            else if (db.currentStaffTeam[i] == 20)
            {
                Instantiate(
                    staffPrefab4, 
                    staffs[i].gameObject.transform.position, 
                    Quaternion.identity);
                staffs[i].SetActive(false);
                wageText[i].text = db.currentStaffTeam[i] + "$";
            }
            else if (db.currentStaffTeam[i] == 0)
            {
                staffs[i].SetActive(false);
                wageText[i].gameObject.SetActive(false);
            }
        }
    }
}
