﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class SprintTracker : MonoBehaviour
{
    public TextMeshPro SprintNoText;
    //public TextMeshPro SprintHourText;
    public Database db;

    void Start()
    {
        db = FindObjectOfType<Database>();
    }

    void Update()
    {
        SprintNoText.text = "Sprint " + (db.currentSprint + 1) + "\n" +  
                            db.sprints[db.currentSprint] + " ManHours left" + "\n" +  
                            db.projectCash + " $ left";
    }
}
