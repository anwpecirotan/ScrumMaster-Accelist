﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneMover : MonoBehaviour
{
    public AudioControls ac;

    void Start() {
        ac = FindObjectOfType<AudioControls>();
    }

    public void LoadScene (int sceneNumber){
        ac.PlaySFX(1, ac.med_vol);
        SceneManager.LoadScene(sceneNumber);
    }

    public void LoadNextScene() {
        ac.PlaySFX(1, ac.med_vol);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void LoadPrevScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }

    public void QuitGame() {
        Application.Quit();
    }
}
