﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

public class NewDay : MonoBehaviour
{
    public Database db;
    public AudioControls ac;
    
    public TextMeshPro statusDisplay;
    public TextMeshPro moneyText;
    public TextMeshPro dailyExpense;
    public TextMeshPro dayText;
    public TextMeshPro sprintText;
    public TextMeshPro hourText;


    void Start()
    {
        db = FindObjectOfType<Database>();
        ac = FindObjectOfType<AudioControls>();
        db.newDay = false;
        if(db.dayCounter == 1)
        {
            db.dayCounter = 0;
        }
        UpdateStatus();

        //solution? yeap!
       
        //db.dayCounter = 0;
       
        db.reduced = false;
    }

    void Update()
    {
        if(db.newDay == true){
            Proggression();
            db.newDay = false;
        }
    }

    public void Proggression()
    {
        db.sprints[db.currentSprint] = 0;
        //reduce backlog
        for (int i = 0; i < db.CurrentPostwMark.Length; i++)
        {
            // <---- problem appears here when there is more than 1 backlog done, dunno why, probably also because ABL has 0 values
            if (db.assignedBackLog[db.CurrentPostwMark[i]] != 0)
            {
                db.currentBackLog[db.CurrentPostwMark[i]] -= db.currentStaffTeam[db.assignedBackLog[db.CurrentPostwMark[i]] - 1];
            }
        }
        CalculateSprintHour();
        UpdateStatus();
        db.doneReduce = true;
    }

    public void printDisplay()
    {
        //replace UI here
        moneyText.text = "" + db.projectCash;
        dayText.text = "" + db.dayCounter;
        if (db.dayCounter > 20)
        {
            dayText.color = Color.red;
        }
        else {
            dayText.color = Color.blue;
        }
        sprintText.text = "Sprint no. " + (db.currentSprint + 1);
        hourText.text = db.sprints[db.currentSprint] + " Hours left";
        int dailyExpenseInt = 0;

        for(int allStaffPay=0; allStaffPay<db.currentStaffTeam.Length; allStaffPay++)
        {
            dailyExpenseInt += db.mirrorStaffTeam[allStaffPay];
        }
        dailyExpense.text = "Daily Expense : \n" 
                            + dailyExpenseInt + " $ /Day";
        /*
        statusDisplay.text = "Project Cash : " + db.projectCash + " $" +
                            "\t\tSprint No : " + (db.currentSprint + 1) +
                            "\nHours Left     : " + db.sprints[db.currentSprint] +
                            "\t\tDay          : " + (db.dayCounter) + " / 30";
         */
    }
    public void UpdateStatus()
    {
       
        db.totalManHour = 0;
        db.dayCounter++;
        for (int i = 0; i < db.staffCount; i++ )
        {
            db.projectCash -= db.mirrorStaffTeam[i];
        }
        for (int i = 0; i < db.sprints.Length; i++ )
        {
            db.totalManHour += db.sprints[i];
        }
           
        printDisplay();
         
        if (db.totalManHour <= 0)
        {
            //ProjectFinished --> player won
            //move somewhere else? add condition?
            SceneManager.LoadScene(7);
        }
        if (db.dayCounter > 30 || db.projectCash <= 0)
        {
            //player Lost
            SceneManager.LoadScene(6);
        }
    }

    public void CalculateSprintHour()
    {
        for (int i = 0; i < db.currentBackLog.Length; i++)
        {
            if (db.currentBackLog[i] >= 0)
            {
                db.sprints[db.currentSprint] += db.currentBackLog[i];
            }
        }
        //if (db.sprints[db.currentSprint] <= 0)
        //{
        //    //new sprint protocol
        //    db.currentSprint++;
        //    db.SoftReset();
        //    SceneManager.LoadScene(2);
        //}
    }
}
