﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AudioControls : MonoBehaviour
{
    //public Database db;

    public AudioSource[] bgm;
    public AudioSource[] sfx;
    //public GameObject muteBar;

    public bool muted;
    public bool playing;

    public float lo_vol;
    public float med_vol;
    public float hi_vol;
    public float max_vol;

    public static AudioControls instance;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            //return;
        }
        DontDestroyOnLoad(gameObject);
        SceneManager.sceneLoaded += OnSceneLoaded;
        //sets initial volume
        lo_vol = 0.2f;
        med_vol = 0.5f;
        hi_vol = 0.8f;
        max_vol = 1f; 
    }

    //function called everytime a scene is loaded
    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {

        //plays the second bgm on scene 3 - others
        if(playing == true){
            if (scene.buildIndex > 2 && scene.buildIndex < 8)
            {
                playing = false;
                StopBGM(0);
                PlayBGM(1, lo_vol);
            }
        }

        //plays the first bgm on scene 0 - 2
        if(playing == false){
            if (scene.buildIndex <= 2 || scene.buildIndex == 8)
            {
                StopBGM(1);
                PlayBGM(0, lo_vol);
            }
        }
    }

    void Start()
    {
        muted = false;
        playing = true;
    }

    void Update()
    {
    }

    public void PlaySFX(int sfxIndex, float volume)
    {
        sfx[sfxIndex].volume = volume;
        sfx[sfxIndex].Play();
    }

    public void PlayBGM(int sfxIndex, float volume)
    {
        bgm[sfxIndex].volume = volume;
        bgm[sfxIndex].Play();
    }

    public void StopBGM(int sfxIndex)
    {
        bgm[sfxIndex].Stop();
    }

    public void StopSFX(int sfxIndex)
    {
        sfx[sfxIndex].Stop();
    }

    public void mute() {
        //find mutebar
        
        if (muted == false)
        {
            PlayerPrefs.SetInt("mute", 1);
            //muteBar.SetActive(true);
            muted = true;
            lo_vol = 0f;
            med_vol = 0f;
            hi_vol = 0f;
            max_vol = 0f;
            bgm[0].Pause();
        }
        else {
            PlayerPrefs.SetInt("mute", 0);
            //muteBar.SetActive(false);
            muted = false;
            lo_vol = 0.2f;
            med_vol = 0.5f;
            hi_vol = 0.8f;
            max_vol = 1f;
            bgm[0].UnPause();
        }
    }
}
