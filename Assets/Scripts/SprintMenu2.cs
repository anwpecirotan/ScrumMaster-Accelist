﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class SprintMenu2 : MonoBehaviour
{
    

    public int[] productBacklogs;
    public GameObject[] postImageGameObj;

    public GameObject[] staffHolder;
    public GameObject[] allPosts;
    public TextMeshPro[] IdxTextMesh;

    public Database db;

    void Start()
    {
        db = FindObjectOfType<Database>();
        productBacklogs = db.currentBackLog;
        setValues();
    }


    void Update()
    {
        
    }

    public void setValues() {
        for (int i = 0; i < 20; i++)
        {
            IdxTextMesh[i].text = "" + (i + 1);
            //txtPoint[i].text = productBacklogs[i].ToString() + " Hours";
            //index[i].text = i + 1 + "";
            if (productBacklogs[i] == 200)
            {
                Instantiate(postImageGameObj[3], allPosts[i].gameObject.transform.position, Quaternion.identity );
                Destroy(allPosts[i].gameObject);
                staffHolder[i].tag = "post200";
            }
            else if (productBacklogs[i] == 150)
            {
                Instantiate(postImageGameObj[2], allPosts[i].gameObject.transform.position, Quaternion.identity);
                Destroy(allPosts[i].gameObject);
                staffHolder[i].tag = "post150";
            }
            else if (productBacklogs[i] == 100)
            {
                Instantiate(postImageGameObj[1], allPosts[i].gameObject.transform.position, Quaternion.identity);
                Destroy(allPosts[i].gameObject);
                staffHolder[i].tag = "post100";
            }
            else if (productBacklogs[i] == 50)
            {
                Instantiate(postImageGameObj[0], allPosts[i].gameObject.transform.position, Quaternion.identity);
                Destroy(allPosts[i].gameObject);
                staffHolder[i].tag = "post50";
            }
            else if (productBacklogs[i] == 0)
            {
                Destroy(allPosts[i].gameObject);
                IdxTextMesh[i].gameObject.SetActive(false);
                staffHolder[i].SetActive(false);
            }
        }
    }
}
