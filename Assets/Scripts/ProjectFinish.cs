﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ProjectFinish : MonoBehaviour
{
    public Database db;
    public AudioControls ac;
    public Text moneyEarned;
    public Text daySpent;

    void Start()
    {
        db = FindObjectOfType<Database>();
        ac = FindObjectOfType<AudioControls>();
        moneyEarned.text = "You Have Earned " + db.projectCash + "$";
        daySpent.text = "Project Finished with " + db.allDaySpent + " Days";
        ac.PlaySFX(4,ac.lo_vol);
    } 

    void Update()
    {
        
    }

    public void NewProject()
    {
        db.HardReset(); 
    }
}
