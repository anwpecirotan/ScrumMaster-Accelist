﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScanBox : MonoBehaviour
{
    public bool startScan = false;
    public bool stopPrint = false;
    public int[] employeeType;
    public int staffCount;
    private Vector2 initialPos;
    public GameObject confirmPanel;
    public GameObject warningText;
    public Database db;

    void Start()
    {
        db = FindObjectOfType<Database>();
        db.firstTime = false;
        staffCount = 0;
        employeeType = new int[6];
        initialPos = transform.position;
    }

    void Update()
    {
        //moveright
        if(gameObject.transform.localPosition.x < 15 && startScan == true){
            transform.position += transform.right * Time.deltaTime * 100;
        }

        //scan
        if(gameObject.transform.localPosition.x >= 15 && stopPrint == false){           
            

            if (staffCount >= 4 && startScan == true)
            { 
                togglePanelON();
                startScan = false;
                gameObject.transform.position = initialPos;              
            }

            stopPrint = true;
            if(staffCount < 4){
                warningText.SetActive(true);
                //Debug.Log("not enough staff");        
                gameObject.transform.position = initialPos;
                startScan = false;
                staffCount = 0;
                stopPrint = false;
            }
        }
    }

    //called by button
    public void ScanStaff() {
        for (int i = 0; i < employeeType.Length; i++)
        {
            employeeType[i] = 0;
        }
        staffCount = 0;
        startScan = true;
        
    }

    public void togglePanelON() {
        confirmPanel.SetActive(true);
    }

    public void togglePanelOFF()
    {
        stopPrint = false;
        confirmPanel.SetActive(false);
    }

    //called by button
    public void SaveTeam() {
        if (staffCount >= 4)
        {
            db.currentStaffTeam = employeeType;
            db.staffCount = staffCount;

            for (int i = 0; i < staffCount; i++)
            {
                db.mirrorStaffTeam[i] = db.currentStaffTeam[i];
            }

            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
        else {
            Debug.Log("Cant Confirm");
        }
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
       if (coll.gameObject.tag == "staff")
        {       
            employeeType[staffCount] = 5;
            staffCount++;
        }
         if (coll.gameObject.tag == "staff2")
        {
            employeeType[staffCount] = 10;
            staffCount++;
        }
         if (coll.gameObject.tag == "staff3")
        {
            employeeType[staffCount] = 15;
            staffCount++;
        }
         if (coll.gameObject.tag == "staff4")
        {
            employeeType[staffCount] = 20;
            staffCount++;
        }
    }
}
