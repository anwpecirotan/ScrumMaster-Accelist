﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScanBox2 : MonoBehaviour
{
    public bool startScan = false;
    public bool stopPrint = false;
    public int[] AssignedBackLog;
    public int staffCount;
    public int postCount;
    public int scanCount;
    public GameObject ConfirmPanel;
    public Button checkButton;
    public GameObject WarningText;
    public Database db;
    private Vector2 initialPos;

    void Start()
    {
        staffCount = 0;
        postCount = 0;
        scanCount = 0;
        AssignedBackLog = new int[20];
        initialPos = transform.position;
        db = FindObjectOfType<Database>();

       
    }

    void Update()
    {
        if (startScan == true)
        {
            checkButton.interactable = false;
            if (gameObject.transform.position.y >= -3.4f)
            {
                transform.position += new Vector3(0,1) * Time.deltaTime * -10;
            }
            if (gameObject.transform.position.y <= -3.4f && scanCount != 1)
            {
                scanCount++;
            }
            
            if (scanCount > 0 && startScan == true)
            {   
                scanCount = 0;
                startScan = false;
                gameObject.transform.position = initialPos;

                if (staffCount == postCount)
                {
                    PanelON();
                }
                else if (staffCount != postCount)
                {                 
                    gameObject.transform.position = initialPos;
                    WarningText.SetActive(true);
                    Debug.Log(staffCount + " n " + postCount);
                }
                checkButton.interactable = true;
            }         
        }
    }

    public void SaveVars() {
        db.assignedBackLog = AssignedBackLog;
    }

    public void PanelON() {
        ConfirmPanel.gameObject.SetActive(true);
    }

    public void PanelOFF() {
        ConfirmPanel.gameObject.SetActive(false);
    }

    public void StartScan() {
        startScan = true;
        staffCount = 0;
        postCount = 0;
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        //insert stafftype into array
        if (coll.gameObject.tag == "ColorIndex1")
        {
            AssignedBackLog[staffCount] = 1;
            staffCount++;
        }
        else if (coll.gameObject.tag == "ColorIndex2")
        {
            AssignedBackLog[staffCount] = 2;
            staffCount++;
        }
        else if (coll.gameObject.tag == "ColorIndex3")
        {
            AssignedBackLog[staffCount] = 3;
            staffCount++;
        }
        else if (coll.gameObject.tag == "ColorIndex4")
        {
            AssignedBackLog[staffCount] = 4;
            staffCount++;
        } 
        else if (coll.gameObject.tag == "ColorIndex5")
        {
            AssignedBackLog[staffCount] = 5;
            staffCount++;
        } 
        else if (coll.gameObject.tag == "ColorIndex6")
        {
            AssignedBackLog[staffCount] = 6;
            staffCount++;
        }

        //count post num to compare to staff num -> maybe unnecessary as there is already backlog array length stored in database
        if (coll.gameObject.tag == "post200" || 
            coll.gameObject.tag == "post150" || 
            coll.gameObject.tag == "post100" || 
            coll.gameObject.tag == "post50")
        {
            postCount++;
        }
    }
}
