﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Chattable : MonoBehaviour
{
    //script that manages the staff dialogue system in scene 5
    public TextMeshPro dialogueText;

    [TextArea(2,10)]
    public string[] sentences;

    public GameObject[] chatBoxes;
    public int talkCounter;

    private int index;
    public float typingSpd;
    public GameObject boardButton;
    public bool doneTalking;
    public Database db;
    public AudioControls ac;
    public BoardButton bb;
    public int dialogueVers;

    void Start()
    {
        dialogueVers = 0;
        db = FindObjectOfType<Database>();
        bb = FindObjectOfType<BoardButton>();
        ac = FindObjectOfType<AudioControls>();
        doneTalking = false;
        StartCoroutine(Type(0));
        db.talkedWith = new bool[db.staffCount];
    }

    void Update()
    {

    }

    public void ResetDay() {
        foreach(GameObject go in chatBoxes){
            go.SetActive(false);
        }
        bb.MoveToStaff();
        boardButton.SetActive(false);
        db.talkedWithAll = false;
        db.talkedWith = new bool[db.staffCount];
        StartCoroutine(Type(0));
    }

    //dont forget to reset the stuffs here
    public void DeactivateChatBox(int index) {
        //Debug.Log("test");
        //only activate chatbox for the talking staff
        for (int i = 0; i < chatBoxes.Length; i++ )
        {
            chatBoxes[i].SetActive(false);
        }
        chatBoxes[index].SetActive(true);

        //scrumboard button activation validation check below
        db.talkedWith[index] = true;
        db.talkedWithAll = true;
        foreach(bool talked in db.talkedWith){
            if(!talked){
                db.talkedWithAll = false;
                break;
            }
        }

        //enable scrumboard Access button
        if(db.talkedWithAll == true){
            boardButton.SetActive(true);
        }
    }

    public void LoadSentence(int sentence_idx){
        //StopCoroutine("Type");
        StopAllCoroutines();
        doneTalking = true;
        if(doneTalking == true){
            doneTalking = false;
            StartCoroutine(Type(sentence_idx));
        }
    }

    public IEnumerator Type(int sentence_idx){
        if(sentence_idx == 0){
            dialogueVers = 0;
        }
        else if (db.rollResult[sentence_idx - 1] <= 9)
        {
            //problem
            if (db.rollResult[sentence_idx - 1] <= 5)
            {
                dialogueVers = 3;
            }
            else
            {
                dialogueVers = 4;
            }
        }
        else if(db.rollResult[sentence_idx - 1] > 9)
        {
            //no problem 
            if (db.rollResult[sentence_idx - 1] <= 20)
            {
                dialogueVers = 1;
            }
            else
            {
                dialogueVers = 2;
            }
        }

        if (sentence_idx != 0 && db.currentBackLog[db.CurrentPostwMark[sentence_idx - 1]] <= 0) {
            dialogueVers = Random.Range(5, 7);
        }

        if (sentence_idx != 0 && db.staffTasks[sentence_idx - 1] <= 0)
        {
            dialogueVers = Random.Range(7, 10);
        }

        
        dialogueText.text = "";
        ac.PlaySFX(3, ac.med_vol);
        foreach(char letter in sentences[dialogueVers].ToCharArray()){
            dialogueText.text += letter;
            yield return new WaitForSeconds(typingSpd);
        }
        ac.StopSFX(3);
        doneTalking = true;
    }

    #region unused function
    //public void NextSentence() {
    //    Debug.Log("beep");
    //    btn.SetActive(false);
    //    if (index < sentences.Length - 1)
    //    {
    //        index++;
    //        dialogueText.text = "";
    //        StartCoroutine(Type(index));
    //    }
    //    else {
    //        dialogueText.text = "";
    //        btn.SetActive(false);
    //    }
    //}
    #endregion
}
