﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AssignToPosts : MonoBehaviour
{
    public AudioControls ac;
    public CircleCollider2D firstCollider;
    public BoxCollider2D secondCollider;

    private float deltaX, deltaY;
    private Vector2 mousePosition;
    private Vector2 initialPosition;

    //Ukuran No
    public Vector3 startSize;
    public Vector3 clickedSize;

    //Target dimana objek harus diletakkan
    private GameObject targetObject;

    //Apakah objek sudah di posisi target
    public bool onTarget;

    //Apakah objek sudah terkunci di posisi target
    public bool locked;

    //replacement variables
    public bool mouseHold;
    public bool replace;
    public float replaceTimer;
    public float timerLength;


    public bool firstGen = true;

    public GameObject selfPrefab;
    public GameObject collidedStaff;
    public SpriteRenderer sr;
    public SpriteRenderer sr2;
    public SpriteRenderer sr3;

    void Start()
    {
        ac = FindObjectOfType<AudioControls>();
        firstCollider = gameObject.GetComponent<CircleCollider2D>();
        secondCollider = gameObject.GetComponent<BoxCollider2D>();
        timerLength = 0.3f;
        initialPosition = transform.position;
        if (firstGen == true)
        {
            firstGen = false;
            Instantiate(selfPrefab, initialPosition, Quaternion.identity);
            Destroy(gameObject);
        }

        startSize = new Vector3(1,1,1);
        clickedSize = startSize * 0.8f;
        locked = false;
        onTarget = false;
        sr = gameObject.GetComponent<SpriteRenderer>();
        firstCollider.enabled = true;
        secondCollider.enabled = false;
    }

    void Update()
    {
        if(locked == true){
            if(replaceTimer <= timerLength){
                replaceTimer += Time.deltaTime;
            }
        }
    }

    private void OnMouseDown()
    {
        mouseHold = true;
        if (locked == false)
        {
            sr.sortingOrder += 3;
            sr2.sortingOrder += 3;
            sr3.sortingOrder += 3;
            deltaX = Camera.main.ScreenToWorldPoint(Input.mousePosition).x - transform.position.x;
            deltaY = Camera.main.ScreenToWorldPoint(Input.mousePosition).y - transform.position.y;
            transform.localScale = clickedSize;
        }
        if (locked == true)
        {
            Destroy(gameObject);
        }
    }

    private void OnMouseDrag()
    {
        mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = new Vector2(mousePosition.x - deltaX, mousePosition.y - deltaY);
    }

    private void OnMouseUp()
    {
        if (replace == true)
        {
            Destroy(collidedStaff);
            replace = false;
        }
        mouseHold = false;
        sr.sortingOrder -= 3;
        sr2.sortingOrder -= 3;
        sr3.sortingOrder -= 3;
        if (!onTarget)
        {
            transform.position = new Vector2(initialPosition.x, initialPosition.y);
            transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        }
        else
        {
            ac.PlaySFX(0, ac.lo_vol);
            transform.position = targetObject.transform.position;
            transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            locked = true;
            firstCollider.enabled = false;
            secondCollider.enabled = true;
            Instantiate(selfPrefab, initialPosition, Quaternion.identity);
        }
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.tag == "post200" ||
           coll.tag == "post150" ||
           coll.tag == "post100" ||
           coll.tag == "post50")
        {
            onTarget = true;
            targetObject = coll.gameObject;
        }
    }

    void OnTriggerStay2D(Collider2D coll)
    {

        if (coll.tag == "post200" ||
           coll.tag == "post150" ||
           coll.tag == "post100" ||
           coll.tag == "post50")
        {
            onTarget = true;
        }
        //replace staff
        if (coll.tag == "staff" && onTarget == true
            || coll.tag == "staff2" && onTarget == true
            || coll.tag == "staff3" && onTarget == true
            || coll.tag == "staff4" && onTarget == true)
        {
            if (locked == false && mouseHold == true)
            {
                collidedStaff = coll.gameObject;
                replace = true;
            }
            if(locked == true){
                if(replaceTimer >= timerLength){
                    //Destroy(gameObject);
                }
            }
        }
    }

    void OnTriggerExit2D(Collider2D coll)
    {
        if (coll.tag == "post200" ||
           coll.tag == "post150" ||
           coll.tag == "post100" ||
           coll.tag == "post50")
        {
            onTarget = false;
            collidedStaff = null;
        }

        if (coll.tag == "staff"
            || coll.tag == "staff2"
            || coll.tag == "staff3"
            || coll.tag == "staff4")
        {
            collidedStaff = null;
        }
    }
}