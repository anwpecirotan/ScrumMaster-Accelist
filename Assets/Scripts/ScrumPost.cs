﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ScrumPost : MonoBehaviour
{
    //script for scene 5

    private float deltaX, deltaY;
    private Vector2 mousePosition;
    [SerializeField]
    private Vector3 initialPosition;

    public Vector3 startSize;
    public Vector3 clickedSize;

    public GameObject targetObject;

    public bool onTarget;
    public bool stacking;

    public SpriteRenderer sr;
    public SpriteRenderer sr2;
    public AudioControls ac;
    public Database db;
    public ProgressionPanel progProbSee;
    public int indexPost;
    public GameObject frameProgress;

    public float startPoint;
   
    void Start()
    {
        db = FindObjectOfType<Database>();
        ac = FindObjectOfType<AudioControls>();
        initialPosition = gameObject.transform.position;
        startSize = new Vector3(gameObject.transform.localScale.x, gameObject.transform.localScale.y, gameObject.transform.localScale.z);
        clickedSize = startSize * 1.2f;

    }


    void Update()
    {
        //print(db.isViewing);
    }

    private void OnMouseDown()
    {
        if (db.isViewing == false)
        {
            if (db.doneScan == false)
            {
                initialPosition = transform.position;
                sr.sortingOrder += 3;
                sr2.sortingOrder += 3;
                deltaX = Camera.main.ScreenToWorldPoint(Input.mousePosition).x - transform.position.x;
                deltaY = Camera.main.ScreenToWorldPoint(Input.mousePosition).y - transform.position.y;
                transform.localScale = clickedSize;
            }
        }
    }

    private void OnMouseDrag()
    {
        if (db.isViewing == false)
        {


            if (db.doneScan == false)
            {
                mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                transform.position = new Vector2(mousePosition.x - deltaX, mousePosition.y - deltaY);
            }
        }
    }

    private void OnMouseUp()
    {
        if (db.isViewing == false)
        {
            //post is clicked to be seen
            if (gameObject.transform.position.x - initialPosition.x < 0.3f  &&
                gameObject.transform.position.x - initialPosition.x > -0.3f &&
                gameObject.transform.position.y - initialPosition.y < 0.3f &&
                gameObject.transform.position.y - initialPosition.y > -0.3f)
            //if (gameObject.transform.position.x == initialPosition.x && gameObject.transform.position.y == initialPosition.y)
            {             
                db.isViewing = true;
                frameProgress.SetActive(true);

                progProbSee = FindObjectOfType<ProgressionPanel>();
                progProbSee.totalAlreadyCleared = db.mirrorBackLog[indexPost] - db.currentBackLog[indexPost];
                progProbSee.totalManHour = db.mirrorBackLog[indexPost];
                progProbSee.idxTask = indexPost;
                progProbSee.idxWorker = db.assignedBackLog[indexPost];
            
                if (progProbSee.isProgress == false)
                {
                    progProbSee.totalCleared = progProbSee.totalAlreadyCleared;
                }

                for (int i = 0; i < db.CurrentPostwMark.Length; i++)
                {
                    if (indexPost == db.CurrentPostwMark[i])
                    {
                        if (db.rollResult[i] > 9)
                        {
                            progProbSee.status = "On Progress";
                        }
                        else if (db.rollResult[i] <= 9)
                        {
                            progProbSee.status = "Obstacle";
                            progProbSee.idxProblem = db.rollResult[i];          
                        }
                        else
                        {
                            progProbSee.status = "Done or Not Worked yet";
                        }
                    }
                }
            }
            if (db.doneScan == false)
            {
                //hover over other objects
                sr.sortingOrder -= 3;
                sr2.sortingOrder -= 3;
                if (!onTarget || stacking == true)
                {
                    ac.PlaySFX(0, ac.lo_vol);
                    //back to initial pos
                    transform.position = new Vector2(initialPosition.x, initialPosition.y);
                    transform.localScale = startSize;
                }
                else
                {
                    ac.PlaySFX(0, ac.lo_vol);
                    //new position
                    transform.position = targetObject.transform.position;
                    transform.localScale = startSize;
                }
            }     
        }
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.tag == targetObject.tag)
        {
            onTarget = true;
            targetObject = coll.gameObject;
        }
    }

    void OnTriggerStay2D(Collider2D coll)
    {
        if(coll.tag == "post50" ||
            coll.tag == "post100" ||
            coll.tag == "post150" ||
            coll.tag == "post200")
        {
            stacking = true;
        }
    }

    void OnTriggerExit2D(Collider2D coll)
    {
        if (coll.tag == "post50" ||
            coll.tag == "post100" ||
            coll.tag == "post150" ||
            coll.tag == "post200")
        {
            stacking = false;
        }

    }

    
}
